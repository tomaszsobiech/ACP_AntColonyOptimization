import math
import numpy as np
import random
import copy
import plotting

class Environment:
    def __init__(self, number_of_ants, cities_dict, generations, pheromone_ratio,distance_ratio):
        self.number_of_ants = number_of_ants
        self.cities_dict = cities_dict
        self.generations = generations
        self.cities_list = list(cities_dict.keys())
        self.pheromone_matrix = 0
        self.AntsPopulation = []
        self.adj = 0
        self.lines_size_list = []
        self.pheromone_ratio = pheromone_ratio
        self.distance_ratio = distance_ratio
        self.init_cost_and_pheromone_matrix()


    def init_cost_and_pheromone_matrix(self):
        self.cost_matrix = np.array([[calculate_distances_in_matrix(cords_1, cords_2)
                                     for city_1, cords_1 in self.cities_dict.items()]
                                     for city_2, cords_2 in self.cities_dict.items()])

        for i in range(self.cost_matrix.shape[0]):
            for j in range(self.cost_matrix.shape[1]):
                self.cost_matrix[i][j] **= 5

        #cost matrix normalization
        cost_matrix_normalized = copy.deepcopy(self.cost_matrix)
        cost_matrix_flatten = cost_matrix_normalized.flatten()
        for i, row in enumerate(self.cost_matrix):
            min_in_row = min(cost_matrix_flatten)
            max_in_row = max(cost_matrix_flatten)
            target_min = 0.0
            target_max = 100.0
            for j, value in enumerate(row):
                new_value = (value - min_in_row) * (target_max - target_min) / (max_in_row - min_in_row) + target_min
                cost_matrix_normalized[i][j] = new_value
        self.cost_matrix = cost_matrix_normalized

        # initialization of pheromone matrix:
        self.pheromone_matrix = np.array([[1 if city_1 != city_2 else 0 for city_1, cords_1 in self.cities_dict.items()]
                                          for city_2, cords_2 in self.cities_dict.items()])

        adj_list = []
        for i in range(len(self.cities_dict.values())):
            for j in range(len(self.cities_dict.values())):
                element = [i, j]
                element_reverse = [j, i]
                if i != j and element not in adj_list and element_reverse not in adj_list:
                    adj_list.append(element)
        self.adj = np.array(adj_list)
        pass

    def create_ant_population(self):
        for ant in range(self.number_of_ants):
            # random start:
            self.AntsPopulation.append(Ant(random.choice(list(self.cities_dict.keys())), self.cities_dict, self.pheromone_ratio, self.distance_ratio))
            # start always from 'A':
            #self.AntsPopulation.append(Ant('A', self.cities_dict, self.pheromone_ratio, self.distance_ratio))
        pass

    def simulate_generations(self):
        self.create_ant_population()
        lines_size_list = []
        for generation in range(self.generations):
            print(f"generation = {generation}")
            for ant in self.AntsPopulation:
                while(len(ant.visited_list) != len(self.cities_dict.keys())):
                    ant.calculate_all_targets_probability(self.cost_matrix, self.pheromone_matrix, self.cities_list, self.pheromone_ratio, self.distance_ratio)
                    ant.select_next_place()
                    # ant.visited_list.append(ant.actual_place)
                    if ant.actual_place not in ant.visited_list:
                        ant.visited_list.append(ant.actual_place)
                ant.actual_place = ant.visited_list[0] #add first place (back to initial place)
                ant.visited_list.append(ant.actual_place)
                ant.calculate_tour_length(self.cost_matrix, self.cities_list)
            self.update_pheromone_matrix_with_vaporization()

            #cleaning
            for ant in self.AntsPopulation:
                ant.visited_matrix = []
                ant.visited_list = [ant.init_place]
                ant.visited_matrix = np.zeros([len(self.cities_dict), len(self.cities_dict)])
                ant.next_probabilities_and_cities_list = []

            list_tmp = []

            max_pheromone = max(self.pheromone_matrix.flatten())
            for (i, j) in self.adj:
                list_tmp.append(self.pheromone_matrix[i][j] / max_pheromone + self.pheromone_matrix[j][i] / max_pheromone)
            lines_size_list.append(list_tmp)

        return lines_size_list, self.adj, self.cities_dict, self.cost_matrix

    @staticmethod
    def visualization(lines_size_list, possible_ways, cities_dict, cost_matrix):
        lines_size_list_normalized = copy.deepcopy(lines_size_list)
        for i, row in enumerate(lines_size_list):
            min_in_row = min(row)
            max_in_row = max(row)
            target_min = 0.0
            target_max = 20.0
            for j, value in enumerate(row):
                new_value = (value - min_in_row) * (target_max - target_min) / (max_in_row - min_in_row) + target_min
                lines_size_list_normalized[i][j] = new_value

        # without normalization:
        # lines_size_list_normalized = self.lines_size_list
        command = input('Wyswietlic symulacje?\n\r')
        if command == 'y':
            plotting.plotting(lines_size_list_normalized, cities_dict, possible_ways)
        else:
            pass

    def update_pheromone_matrix_without_vaporization(self):
        new_pheromone_value_matrix = np.zeros([len(self.cities_dict), len(self.cities_dict)])
        for ant in self.AntsPopulation:
            for i, row in enumerate(self.pheromone_matrix):
                for j, element in enumerate(row):
                    if ant.visited_matrix[i][j] == 1:
                        new_pheromone_value_matrix[i][j] = self.pheromone_matrix[i][j] + 1 / ant.tour_length #w ith vaporization
        self.pheromone_matrix = copy.deepcopy(new_pheromone_value_matrix)
        pass

    def update_pheromone_matrix_with_vaporization(self):
        new_pheromone_value_matrix = np.zeros([len(self.cities_dict), len(self.cities_dict)])
        for ant in self.AntsPopulation:
            for i, row in enumerate(self.pheromone_matrix):
                for j, element in enumerate(row):
                    if ant.visited_matrix[i][j] == 1:
                        new_pheromone_value_matrix[i][j] = (1 - 0.3) * self.pheromone_matrix[i][j] + 1 / ant.tour_length #w ith vaporization
        self.pheromone_matrix = copy.deepcopy(new_pheromone_value_matrix)
        pass


class Ant:
    def __init__(self, init_place, cities_dict, pheromone_ratio, distance_ratio):
        self.tour_length = 0
        self.init_place = init_place
        self.pheromone_ratio = pheromone_ratio
        self.distance_ratio = distance_ratio
        self.actual_place = init_place
        self.visited_list = [init_place]
        self.visited_matrix = np.zeros([len(cities_dict), len(cities_dict)])
        self.next_probabilities_and_cities_list = []

    def select_next_place(self):
        spin_the_wheel = random.random()
        sorted_probabilities_and_cities_list = sorted(self.next_probabilities_and_cities_list, key=lambda tup: tup[1])
        part = 0
        sum_probabilities = sum(x[1] for x in sorted_probabilities_and_cities_list)
        for probability_and_city in sorted_probabilities_and_cities_list:
            part += probability_and_city[1]/sum_probabilities
            if part > spin_the_wheel:
                self.actual_place = probability_and_city[0]
                break
        pass

    def calculate_tour_length(self, cost_matrix, cities_list):
        previous_city = self.init_place
        tour_len = 0
        for city in self.visited_list[1:]:
            city_from_index = cities_list.index(previous_city)
            city_to_index = cities_list.index(city)
            tour_len += cost_matrix[city_from_index][city_to_index]
            self.visited_matrix[city_from_index][city_to_index] = 1
            previous_city = city
        self.tour_length = tour_len

    def calculate_this_target_probability(self, target, cost_matrix, pheromone_matrix, cities_list, pheromone_ratio, distance_ratio):
        start_place_index = cities_list.index(self.actual_place)
        target_index = cities_list.index(target)

        other_cities_indexes = [cities_list.index(city) for city in cities_list if city is not self.actual_place and city is not target]

        denominator = [(pheromone_matrix[start_place_index][target_idx]) ** pheromone_ratio *
                       (1 / cost_matrix[start_place_index][target_idx]) ** distance_ratio
                       for target_idx in other_cities_indexes]

        probability = (((pheromone_matrix[start_place_index][target_index]) ** pheromone_ratio *
                        (1 / cost_matrix[start_place_index][target_index])) ** distance_ratio) / \
                      ((pheromone_matrix[start_place_index][target_index]) ** pheromone_ratio *
                       (1 / cost_matrix[start_place_index][target_index]) ** pheromone_ratio + sum(denominator))


        if probability == 0:
            probability = 0.00000001
        return probability

    def calculate_all_targets_probability(self, cost_matrix, pheromone_matrix, cities_list, pheromone_ratio, distance_ratio):
        # jesli zakladam ze mrowka moze wracac do odwiedzonych miejsc:
        # self.next_probabilities_and_cities_list = [(target, self.calculate_this_target_probability(target, cost_matrix,
        #                                                                          pheromone_matrix, cities_list))
        #                                            for target in cities_list if target is not self.actual_place]
        # jesli zakladam ze mrowka NIE MOŻE wracac do odwiedzonych miejsc:
        self.next_probabilities_and_cities_list = [(target, self.calculate_this_target_probability(target, cost_matrix,
                                                                                 pheromone_matrix, cities_list, pheromone_ratio, distance_ratio))
                                                   for target in cities_list if target not in self.visited_list]

def calculate_distance_between_cities(A, B):
    distance = math.sqrt(math.pow((A[0]-B[0]), 2) + math.pow((A[1]-B[1]), 2))
    return distance


def calculate_distances_in_matrix(A, B):
    return 0 if A == B else calculate_distance_between_cities(A, B)