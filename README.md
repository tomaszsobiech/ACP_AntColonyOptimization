# Simpe Ant Colony Optimization project.


## Running
1. python 3.7
2. pip install -r requirements.txt
3. python run.py [generations] [number_of_ants] [pheromone_ratio] [distance_ratio]

I recommend the following settings:
* generations ( > 100); 
* number of ants ( > 100);
* pheromone_ratio [0, 1]; 
* distance_ratio ( > 2).

My implementation is looking for the shortest  route connecting all cities.
#40 generations simulation:

![](img/colony_run.gif)


For more info:

[Ant_colony_optimization_algorithms](https://en.wikipedia.org/wiki/Ant_colony_optimization_algorithms)

![](img/ant_graph.svg.png)
[Source](https://www.sciencedirect.com/science/article/pii/S0142061515005840)


# Todo:
- different cost depending on the direction (e.g. AB = 10, BA = 1)