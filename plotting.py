import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import copy

cnt = 0

class Graph(pg.GraphItem):
    def __init__(self):
        self.dragPoint = None
        self.dragOffset = None
        self.textItems = []
        pg.GraphItem.__init__(self)
        #self.scatter.sigClicked.connect(self.clicked)

    def setData(self, **kwds):
        self.text = kwds.pop('text', [])
        self.data = kwds
        if 'pos' in self.data:
            npts = self.data['pos'].shape[0]
            self.data['data'] = np.empty(npts, dtype=[('index', int)])
            self.data['data']['index'] = np.arange(npts)
        self.setTexts(self.text)
        self.updateGraph()

    def setTexts(self, text):
        for i in self.textItems:
            i.scene().removeItem(i)
        self.textItems = []
        for t in text:
            item = pg.TextItem(t)
            self.textItems.append(item)
            item.setParentItem(self)

    def updateGraph(self):
        pg.GraphItem.setData(self, **self.data)
        for i, item in enumerate(self.textItems):
            item.setPos(*self.data['pos'][i])


    # def mouseDragEvent(self, ev):
    #     if ev.button() != QtCore.Qt.LeftButton:
    #         ev.ignore()
    #         return
    #
    #     if ev.isStart():
    #         # We are already one step into the drag.
    #         # Find the point(s) at the mouse cursor when the button was first
    #         # pressed:
    #         pos = ev.buttonDownPos()
    #         pts = self.scatter.pointsAt(pos)
    #         if len(pts) == 0:
    #             ev.ignore()
    #             return
    #         self.dragPoint = pts[0]
    #         ind = pts[0].data()[0]
    #         self.dragOffset = self.data['pos'][ind] - pos
    #     elif ev.isFinish():
    #         self.dragPoint = None
    #         return
    #     else:
    #         if self.dragPoint is None:
    #             ev.ignore()
    #             return
    #
    #     ind = self.dragPoint.data()[0]
    #     self.data['pos'][ind] = ev.pos() + self.dragOffset
    #     self.updateGraph()
    #     ev.accept()

    # def clicked(self, pts):
    #     print("clicked: %s" % pts)

def update_variables(g, pos, adj, lines, size, symbols, texts):
    ## Update the graph
    g.setData(pos=pos, adj=adj, pen=lines, size=size, symbol=symbols, pxMode=False, text=texts)


def plotting(lines_size_list, cities_dict, adj):

    # Enable antialiasing for prettier plots
    pg.setConfigOptions(antialias=True)

    w = pg.GraphicsWindow()
    w.setWindowTitle('Generation: 0')
    v = w.addViewBox()
    v.setAspectLocked()

    g = Graph()

    # text = pg.TextItem(
    #     html='<div style="text-align: center"><span style="color: #FFF;">This is the</span><br><span style="color: #FF0; font-size: 16pt;">PEAK</span></div>',
    #     text="Generacja",
    #     anchor=(-0.3, 0.5))
    # v.addItem(text)
    # text.setPos(0, 7)

    v.addItem(g)
    ## Define positions of nodes
    pos = np.array([[x[0], x[1]] for x in cities_dict.values()], dtype=float)
    ## Define the symbol to use for each node (this is optional)
    symbols = ['o' for x in range(len(cities_dict.keys()))]

    lines = np.array([(255, 255, 255, 255, 1) for x in range(adj.shape[0])],
                     dtype=[('red', np.ubyte), ('green', np.ubyte), ('blue', np.ubyte),
                            ('alpha', np.ubyte), ('width', float)])

    ## Define text to show next to each symbol

    texts = list(cities_dict.keys())

    places_size = 0.5 #min_distance/10
    update_variables(g, pos, adj, lines, places_size, symbols, texts)

    timer = pg.QtCore.QTimer()

    def update():
        global cnt
        cnt += 1
        if cnt < len(lines_size_list):
            lines = np.array([(255, 255, 255, 255, lines_size_list[cnt - 1][x]) for x in range(adj.shape[0])],
                             dtype=[('red', np.ubyte), ('green', np.ubyte), ('blue', np.ubyte), ('alpha', np.ubyte),
                                    ('width', float)])

            update_variables(g, pos, adj, lines, places_size, symbols, texts)
            w.setWindowTitle('Generation: '+str(cnt))
        elif cnt == len(lines_size_list):
            lines_last = lines_size_list[-1]

            # searching best roads:
            tmp = copy.deepcopy(lines_last)
            tmp_list = [value+0.001*i for i, value in enumerate(tmp)]
            x_sorted = sorted(tmp_list)
            x_sorted.reverse()
            max_values_idx_array = []
            i = 0
            while(len(max_values_idx_array)<len(cities_dict)):
                if int(tmp_list.index(x_sorted[i])) not in max_values_idx_array:
                    max_values_idx_array.append(tmp_list.index(x_sorted[i]))
                i += 1
            # end searching best roads
            red_color_sign = [[255, 0, 0] if i in max_values_idx_array else [255, 255, 255] for i in range(adj.shape[0])]

            lines = np.array([(red_color_sign[x][0], red_color_sign[x][1], red_color_sign[x][2], 255, lines_size_list[cnt - 1][x]) for x in range(adj.shape[0])],
                             dtype=[('red', np.ubyte), ('green', np.ubyte), ('blue', np.ubyte), ('alpha', np.ubyte),
                                    ('width', float)])
            update_variables(g, pos, adj, lines, places_size, symbols, texts)
            w.setWindowTitle('Generation: ' + str(cnt) + '  Success!')
        else:
            pass

        g.updateGraph()
    timer.timeout.connect(update)
    #timer.setInterval(1000)
    timer.start(11000)

    QtGui.QApplication.instance().exec_()