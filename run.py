import ant_colony
import load_cities
import sys

def run(generations, number_of_ants, pheromone_ratio, distance_ratio):
    cities_dict = load_cities.load()

    Env = ant_colony.Environment(number_of_ants=number_of_ants,
                                 cities_dict=cities_dict,
                                 generations=generations,
                                 pheromone_ratio=pheromone_ratio,
                                 distance_ratio=distance_ratio)

    visualisation_data = Env.simulate_generations()
    ant_colony.Environment.visualization(*visualisation_data)

try:
    generations, number_of_ants, pheromone_ratio, distance_ratio = int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4])

    print(f"generations = {generations}\n\r number_of_ants = {number_of_ants}\n\r pheromone_ratio = {pheromone_ratio}\n\rdistance_ratio = {distance_ratio}\n\r\n\ry")
    run(generations, number_of_ants, pheromone_ratio, distance_ratio)

except Exception as e:
    print(e)
    print("Usage: python run.py [generations] [number_of_ants] [pheromone_ratio] [distance_ratio]"
          "\n\rgenerations > 100,"
          "\n\rnumber_of_ants > 20,"
          "\n\rpheromone_ratio [0, 1],"
          "\n\rdistance_ratio > 2")
    exit()